package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    //added these field-variables
    private final int cols;
    private final int rows;
    private final CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.cols = columns;
        this.rows = rows;
        grid = new CellState[rows][columns];
    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    /**
     * This method, acceptableValue makes a boolean expressian wether the given value for row and col
     * is acceptable, using if-statements.
     * @param row checks if the input row is within the limits of the grid.
     * @param col checks if the input col is within the limits of the grid.
     * @return true, if the parameters is acceptable. Returns false if the method is not accepted.
     */
    private boolean acceptableValue(int row, int col){
        if (row < 0 || row > numRows()) {
            return false;
        }
        if (col < 0 || col > numColumns()) {
            return false;
        }
        return true;
        //return col >= 0 || col > numColumns();
    }

    @Override
    public void set(int row, int column, CellState inputCellstate) {

        if (!acceptableValue(row, column)) {
            throw new IndexOutOfBoundsException();
        }

        grid[row][column] = inputCellstate;
    }

    @Override
    public CellState get(int row, int column) {
        if (!acceptableValue(row, column)){
            throw new IndexOutOfBoundsException();
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid gridCopy = new CellGrid(rows, cols, CellState.DEAD);
        for (int i = 0; i < this.rows; i++) {
            for (int k = 0; k < this.cols; k++) {
                
                CellState temporarily = this.get(i,k);
                gridCopy.set(i, k, temporarily);
            }
        }
        return gridCopy;
    }
    
}
